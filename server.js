var http = require('http');
var fs = require('fs');

var server = http.createServer(function(request, response) {

  var url = request.url.toString();
  console.log('Request Url:', url);

  var contentType = "text/html";
  var data = null;
  var statusCode = 200;

  if (/^.*\.(css)$/.test(url)) {
    contentType = 'text/css';
  }

  try {
    data = fs.readFileSync(url.substring(1) || 'index.html', 'utf8');
  } catch (ex) {
    console.log('Could not find resource');
    statusCode = 404;
  }

  writeToResponse(response, data, statusCode, contentType)

  function writeToResponse(response, data, statusCode, contentType) {
    response.writeHead(statusCode, {
      'Content-Type': contentType
    });
    if (data) {
      response.write(data);
    }
    response.end();
  }
});


server.listen(8080, '127.0.0.1');
console.log('Listening to port 8080 ...');
